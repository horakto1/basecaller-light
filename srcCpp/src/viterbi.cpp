#include "viterbi.h"

using namespace std;
// Viterbi Algorithm

//----------------------------------------------------------------------------------------------------//
// Viterbi
vector<unsigned int> viterbi(vector<double> currents, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, vector< vector<unsigned int> > pred) {
	//First current
	vector<vector<double> > probs(currents.size(), vector<double>(emMeans.size()));
	vector<vector<unsigned int> > prev(currents.size(), vector<unsigned int>(emMeans.size()));
	for (unsigned int st = 0; st < emMeans.size(); ++st) {
		probs[0][st] = logX(normVal(currents[0], emMeans[st].first, emMeans[st].second));
	}
	// Others
	double maxCur, maxProb, tmp;
	unsigned int pos;
	for (unsigned int t = 1; t < currents.size(); ++t) {
		maxCur = -DBL_MAX;
		for (unsigned int st = 0; st < emMeans.size(); ++st) {
			pos = 0;
			maxProb = probs[t-1][pred[st][0]] + getTransPLog(&transM, pred[st][0], st, stay_prob, step_prob, skip_prob);
			for (vector<unsigned int>::iterator prevSt = pred[st].begin() + 1; prevSt != pred[st].end(); ++prevSt) {				
				tmp = probs[t-1][*prevSt] + getTransPLog(&transM, *prevSt, st, stay_prob, step_prob, skip_prob);
				if (tmp > maxProb) {
					maxProb = tmp;
					pos = *prevSt;
				}
			}
			maxProb += logX(normVal(currents[t], emMeans[st].first, emMeans[st].second));
			if (maxProb > maxCur) maxCur = maxProb;
			probs[t][st] = maxProb;
			prev[t][st] = pos;
		}
	}

	unsigned int prevSt = 0;
	double post;
	maxProb = probs[probs.size() - 1][0];
	// Get most probable state and its backtrack
	for (unsigned int st = 1; st < probs[probs.size() - 1].size(); ++st) {
		post = probs[probs.size() - 1][st];
		if (post > maxProb) {
			maxProb = post;
			prevSt = st;
		}
	}
	
	vector<unsigned int> res(probs.size());
	res[res.size() - 1] = prevSt;

	// Bactrack phase
	for (int t = probs.size() - 2; t >= 0; --t) {
		res[t] = prev[t+1][prevSt];
		prevSt = prev[t+1][prevSt];
	}
	return res;
}

//----------------------------------------------------------------------------------------------------//
// Main
int main(int argc, char* argv[]) {
	if (argc < 7 || argc > 7) {
		cout << "Wrong usage! Use " << argv[0] << " currents.fasta table.csv output.fasta size stay_prob skip_prob" << endl;
		return 1;
	}

	string cFile(argv[1]);
	string tFile(argv[2]);
	string oFile(argv[3]);

	unsigned int size = stoi(string(argv[4]));
	double stay_prob = stod(string(argv[5]));
	double skip_prob = stod(string(argv[6]));
	double step_prob = 1 - stay_prob - skip_prob;

	vector<pair<double, double> > emMeans = readTable(tFile, size);
	vector<vector <unsigned int> > pred = findPredecessors(size);
	vector<Table *> transM = createTM(size);

	ifstream inCurr;
	inCurr.open(cFile.c_str());
	
	ofstream out;
	out.open(oFile.c_str());
	
	string label, curs;
	char sep = ',';
	double c = 0;
	vector<double> currents;
	vector<unsigned int> res;
	string res2;
	inCurr >> sep;
	cout << "Viterbi " << size << " " << stay_prob << " " << skip_prob << endl;
	while (inCurr.good()) {
		sep = ',';
		currents.clear();
		inCurr >> label;
		out << ">" << label << endl;

		while(inCurr >> c >> sep && sep == ',') {
			currents.push_back(c);
		}
		currents.push_back(c);
		res = viterbi(currents, transM, emMeans, size, stay_prob, step_prob, skip_prob, pred);
		res2 = consolidate(res, size);
		out << res2 << endl;
	}

	for (unsigned int i = 0; i < transM.size(); ++i) {
		delete transM[i];
	}
	inCurr.close();
	out.close();
	return 0;
}