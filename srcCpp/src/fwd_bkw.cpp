#include "fwd_bkw.h"

using namespace std;

bool reverseCompare(pair<int, double> i, pair<int, double> j) { 
	return (i.second > j.second); 
}

// Forward-Backward
vector<unsigned int> fwdBkw(vector<double> currents, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, double smoothing, vector< vector<unsigned int> > pred, vector< vector<unsigned int> > foll) {
	// Forward part of the algorithm
	string tmpStr;
	// First current
	vector<vector<double> > fwd(currents.size(), vector<double>(emMeans.size()));
	for (unsigned int st = 0; st < emMeans.size(); ++st) {
		fwd[0][st] = normVal(currents[0], emMeans[st].first, emMeans[st].second);
	}
	// Others
	double maxCur, sum, app;
	for (unsigned int t = 1; t < currents.size(); ++t) {
		maxCur = -DBL_MAX;
		for (unsigned int st = 0; st < emMeans.size(); ++st) {
			sum = 0;
			for (vector<unsigned int>::iterator prevSt = pred[st].begin(); prevSt != pred[st].end(); ++prevSt) {				
				sum += fwd[t-1][*prevSt] * getTransP(&transM, *prevSt, st, stay_prob, step_prob, skip_prob);
			}
			app = log(normVal(currents[t], emMeans[st].first, emMeans[st].second) * sum);
			if (app > maxCur) maxCur = app;
			fwd[t][st] = app;
		}
	
		for (unsigned int st = 0; st < fwd[t].size(); ++st) {
			fwd[t][st] = exp(fwd[t][st] - maxCur);
		}
	}

	//Backward part of the algorithm
	//Last current
	vector<vector<double> > bkw(currents.size(), vector<double>(emMeans.size()));
	for (unsigned int st = 0; st < emMeans.size(); ++st) {
		bkw[currents.size() - 1][st] = normVal(currents[currents.size() - 1], emMeans[st].first, emMeans[st].second);
	}
		
	//Others
	vector<double> vals(emMeans.size());
	for (int t = currents.size() - 2; t >= 0; --t) {
		maxCur = -DBL_MAX;
		for (unsigned int nextSt = 0; nextSt < vals.size(); ++nextSt) {
			vals[nextSt] = normVal(currents[t], emMeans[nextSt].first, emMeans[nextSt].second);
		}
			
		for (unsigned int st = 0; st < emMeans.size(); ++st) {
			sum = 0;
			for (vector<unsigned int>::iterator nextSt = foll[st].begin(); nextSt != foll[st].end(); ++nextSt) {				
				sum += bkw[t + 1][*nextSt] * getTransP(&transM, st, *nextSt, stay_prob, step_prob, skip_prob) * vals[*nextSt];
			}
			app = log(sum);
			if (app > maxCur) maxCur = app;
			bkw[t][st] = app;
		}

		for (unsigned int st = 0; st < bkw[t].size(); ++st) {
			bkw[t][st] = exp(bkw[t][st] - maxCur);
		}
	}

	// Start of Smoothing phase
	unsigned int prevSt = 0;
	double post, maxProb = fwd[0][0] * bkw[0][0];
	for (unsigned int st = 1; st < fwd[0].size(); ++st) {
		post = fwd[0][st] * bkw[0][st];
		if (post > maxProb) {
			maxProb = post;
			prevSt = st;
		}
	}
	
	vector<unsigned int> res(fwd.size());
	res[0] = prevSt;

	bool found ;
	int sm = 0,	ns = 0;
	unsigned int st;
	vector<pair<unsigned int, double> > sts;
	for (unsigned int t = 1; t < fwd.size(); ++t) {
		sts.clear();
		for (unsigned int j = 0; j < fwd[t].size(); ++j) {
			sts.push_back(pair<unsigned int, double>(j, fwd[t][j] * bkw[t][j]));
		}
		sort(sts.begin(), sts.end(), reverseCompare);

		st = sts[0].first;
		if (prevSt != st && !follows(prevSt, st, 1, size) && !follows(prevSt, st, 2, size)) {
			for (unsigned int s = 1; s < sts.size(); ++s) {
				if (sts[s].second >= smoothing * sts[0].second) {
					found = false;
					for (unsigned int k = 0; k < size - 1; ++k) {
						if (follows(prevSt, sts[s].first, k, size)) {
							st = sts[s].first;
							found = true;
							break;
						}
					}
					if (found) {
						sm += 1;
						break;
					}
				} else {
					ns += 1;
					break;
				}
			}
		}
		res[t] = st;
		prevSt = st;
	}

	return res;
}

// Main
int main(int argc, char* argv[]) {
	if (argc < 9 || argc > 9) {
		cout << "Wrong usage! Use " << argv[0] << " currents.fasta table.csv output.fasta size stay_prob skip_prob smoothing consolidate" << endl;
		return 1;
	}

	string cFile(argv[1]);
	string tFile(argv[2]);
	string oFile(argv[3]);

	unsigned int size = stoi(string(argv[4]));
	double stay_prob = stof(string(argv[5]));
	double skip_prob = stof(string(argv[6]));
	double step_prob = 1 - stay_prob - skip_prob;
	double smoothing = stof(string(argv[7]));
	char cons = argv[8][0];

	vector<pair<double, double> > emMeans = readTable(tFile, size);
	vector<vector <unsigned int> > pred = findPredecessors(size);
	vector<vector <unsigned int> > foll = findFollowers(size);
	vector<Table *> transM = createTM(size);

	ifstream inCurr;
	inCurr.open(cFile.c_str());
	
	ofstream out;
	out.open(oFile.c_str());
	
	string label, curs;
	char sep = ',';
	double c = 0;
	vector<double> currents;
	vector<unsigned int> res;
	string res2;
	inCurr >> sep;
	sep = ',';
	cout << "FwdBkw " << size << " " << stay_prob << " " << skip_prob << " " << smoothing << " " << cons << endl;
	while (inCurr.good()) {
		currents.clear();
		inCurr >> label;
		out << ">" << label << endl;

		while(inCurr >> c >> sep && sep == ',') {
			currents.push_back(c);
		}
		currents.push_back(c);
		
		res = fwdBkw(currents, transM, emMeans, size, stay_prob, step_prob, skip_prob, smoothing, pred, foll);
		if (cons == 'y') res2 = consolidateFB(res, size);
		else res2 = consolidate(res, size);

		out << res2 << endl;
	}

	for (unsigned int i = 0; i < transM.size(); ++i) {
		delete transM[i];
	}
	inCurr.close();
	out.close();
	return 0;
}
