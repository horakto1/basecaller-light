#include "lib.h"

using namespace std;

//----------------------------------------------------------------------------------------------------//
// Conversions

int bToi(char base) {
	if (base == 'A') return 0;
	if (base == 'C') return 1;
	if (base == 'G') return 2;
	return 3;
}

char iTob(int i) {
	if (i == 0)	return 'A';
	if (i == 1) return 'C';
	if (i == 2) return 'G';
	return 'T';
}

int sToi(string s) {
	if (s.length() == 1) return bToi(s[0]);
	return bToi(s[s.length() - 1]) + (4 * sToi(string(s, 0, s.length() - 1)));
}

string iTos(int i, int l) {
	if (l == 0) return "";
	return iTos(int(i / 4), l - 1) + iTob(i % 4);
}

//----------------------------------------------------------------------------------------------------//
// Reading files

// Reads mean currents for each base l-tuple from csv file
vector<pair<double, double> > readTable(string f, int l) {
	int size = 4;
	for (int i = 0; i < l -1; ++i) size *= 4;

	vector<pair<double, double> > table(size, pair<double, double>(0, 0));
		
	ifstream in(f);
	string value, state;
	char c;
	double first, second;
	int st;
	in >> c;
	while(in.good()) {
		getline (in, value, ',');
		state = string(value, 0, 1);
		for (int i = 1; i < l; ++i) {
			getline (in, value, ',');
			state += string(value, 1, 1);
		}
		st = sToi(state);
		in >> first >> c >> second >> c;
		table[st].first = first;
		table[st].second = second;
	}
	return table;
}
//----------------------------------------------------------------------------------------------------//
// Handling results

string consolidate(vector<unsigned int> seq, int l) {
	string res = "";
	vector<int> moves(1, 0);
	unsigned int prev = seq[0];
	vector<string> res2(0);
	string s = iTos(seq[0], l);
	res2.push_back(s);
	res += s;
	
	int m1;
	for (unsigned int t = 1; t < seq.size(); ++t) {
		s = iTos(seq[t], l);
		res2.push_back(s);

		if (prev == seq[t]) moves.push_back(0);
		else {
			m1 = followsN(prev, seq[t], l);
			moves.push_back(m1);
			for (unsigned int j = s.length() - m1; j < s.length(); ++j) {
				res += s[j];
			}
		}
		prev = seq[t];
	}
	return res;
}

string consolidateFB(vector<unsigned int> seq, int l) {
	string res = "";
	vector<int> moves(1, 0);
	unsigned int prev = seq[0];
	vector<string> res2(0);
	string s = iTos(seq[0], l);
	res2.push_back(s);
	res += s;
	string sNext;
	int m1,m2;

	string tmpStr;
	for (unsigned int t = 1; t < seq.size() - 1; ++t) {
		s = iTos(seq[t], l);
		sNext = iTos(seq[t+1], l);
		res2.push_back(s);

		if (prev == seq[t]) moves.push_back(0);
		else {
			m1 = followsN(prev, seq[t], l);
			if (m1 >= 3 && followsN(seq[t], seq[t+1], l) >= 3) {
				moves.push_back(-1);
				m2 = followsN(prev, seq[t+1], l);
				moves.push_back(m2);
				
				for (unsigned int j = s.length() - m2; j < s.length(); ++j) {
					res += sNext[j];
				}
				++t;
			} else {
				moves.push_back(m1);
				for (unsigned int j = s.length() - m1; j < s.length(); ++j) {
					res += s[j];
				}
			}
		}
		prev = seq[t];
	}
	
	s = iTos(seq[seq.size() - 1], l);
	res2.push_back(s);
	m1 = followsN(prev, seq[seq.size() - 1], l);
	moves.push_back(m1);
	for (unsigned int j = s.length() - m1; j < s.length(); ++j) {
		res += s[j];
	}
		
	return res;
}
//----------------------------------------------------------------------------------------------------//
// Transitions

double getTransP(vector<Table *> * matrices, int prev, int cur, double stay_prob, double step_prob, double skip_prob) {
	double p = 0;
	p += (*matrices)[0] -> get(prev, cur) * stay_prob;
	p += (*matrices)[1] -> get(prev, cur) * step_prob;
	p += (*matrices)[2] -> get(prev, cur) * skip_prob;
	return p;
}

double getTransPLog(vector<Table *> * matrices, int prev, int cur, double stay_prob, double step_prob, double skip_prob) {
	return log(getTransP(matrices, prev, cur, stay_prob, step_prob, skip_prob));
}

// Handles 3+
bool follows(int prev, int next, int move, int l) {
	int d = 1;
	for (int i = 0; i < move; ++i) d *= 4;
	int m = 1;
	for (int i = 0; i < l - move; ++i) m *= 4;
	return (prev % m) == (int)(next / d);
}

int followsN(int prev, int st, int l) {
	if (prev == st) return 0;

	for (int i = 1; i < l; ++i) {
		if (follows(prev, st, i, l)) return i;
	}
	return l;
}

vector<Table *>  createTM(int l) {
	vector<Table *> matrices(0);

	matrices.push_back(new Table(0, l));
	matrices.push_back(new Table(1, l));
	matrices.push_back(new Table(2, l));

	return matrices;
}

bool notIn(vector<unsigned int> vec, unsigned int value) {
	for (unsigned int i = 0; i < vec.size(); ++i) {
		if (vec[i] == value) return false;
	}
	return true;
}

vector<vector<unsigned int> > findFollowers(int l) {
	vector<vector<unsigned int> > followers(0);
	
	unsigned int size = 16;
	unsigned int size1 = 4;
	unsigned int size2 = 1;
	for (int i = 0; i < l-2; ++i) {
		size *= 4;
		size1 *= 4;
		size2 *= 4;
	}

	unsigned int moved;
	for (unsigned int i = 0; i < size; ++i) {
		followers.push_back(vector<unsigned int>(0));

		moved = (i % size2) * 16;
		for (unsigned int j = 0; j < 16; ++j) {
			followers[i].push_back(moved + j);
		}
		moved = (i % size1) * 4;
		for (unsigned int j = 0; j < 4; ++j) {
			if (notIn(followers[i], moved + j)) { 
				followers[i].push_back(moved + j);
			}
		}
		if (notIn(followers[i], i)) {
			followers[i].push_back(i);
		}
	}
	return followers;
}

vector<vector<unsigned int> > findPredecessors(int l) {
	vector<vector<unsigned int> > pred(0);
	
	int size = 16;
	int size1 = 4;
	int size2 = 1;
	for (int i = 0; i < l-2; ++i) {
		size *= 4;
		size1 *= 4;
		size2 *= 4;
	}

	int moved;
	for (int i = 0; i < size; ++i) {
		pred.push_back(vector<unsigned int>(0));

		moved = (int)(i / 16);
		for (int j = 0; j < 16; ++j) {
			pred[i].push_back(moved + (j * size2));
		}
		moved = (int)(i / 4);
		for (int j = 0; j < 4; ++j) {
			if (notIn(pred[i], moved + (j * size1))) { 
				pred[i].push_back(moved + (j * size1));
			}
		}
		if (notIn(pred[i], i)) {
			pred[i].push_back(i);
		}
	}
	return pred;
}

//----------------------------------------------------------------------------------------------------//
// Math

double expX(double x) {
	return exp(x);
}

double logX(double x) {
	if (x <= 0) return -DBL_MAX;
	return log(x);
}

double normVal(double x, double m, double s) {
    static const double inv_sqrt_2pi = 0.3989422804014327;
    double a = (x - m) / s;
    return (inv_sqrt_2pi / s) * exp(-0.5 * a * a);
}

//-----------------------------------------------------------------------------------------------------//
// Class Table

Table::Table(int move, int l) {
	int size = 4;
	for (int i = 0; i < l - 1; ++i) size *= 4;

	matrix = vector<vector<double> >(size, vector<double>(size, 0.0));
	if (move == 0) {
		for (int i = 0; i < size; ++i) {
			matrix[i][i] = 1.0;
		}
	} else {
		double p = 1.0;
		for (int i = 0; i < move; ++i) {
			p /= 4;
		}
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (follows(i, j, move, l)) {
					matrix[i][j] = p;
				}
			}
		}
	}
}


double Table::get(int prev, int next) {
	return matrix[prev][next];
}

void Table::print() {
	cout << "Matrix: " << endl;
	for (unsigned int i = 0; i < matrix.size(); ++i) {
		for (unsigned int j = 0; j < matrix[i].size(); ++j) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}
