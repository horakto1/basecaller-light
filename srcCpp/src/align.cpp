#include "align.h"

using namespace std;

int S(char a, char b) {
	if (a == b) return 1;
	return -1;
}

int gap() {
	return -1;
}

int max3(int t, int tl, int l, bool &T, bool &L) {
	int m = 0;
	if (l >= m) {
		m = l;
		T = false;
		L = true;
	}
	if (t >= m) {
		m = t;
		T = true;
		L = false;
	}
	if (tl >= m) {
		m = tl;
		T = true;
		L = true;
	}
	return m;
}

int min3(int t, int tl, int l, bool &T, bool &L) {
	int m = tl;
	T = true;
	L = true;
	if (t < m) {
		m = t;
		T = true;
		L = false;
	}
	if (l < m) {
		m = l;
		T = false;
		L = true;
	}
	return m;
}

int maxG(int noJump, int jump, bool &jumped) {
	if (noJump > jump) {
		jumped = false;
		return noJump;
	} else {
		jumped = true;
		return jump;
	}
}

//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Needleman-Wunsch Algorithm
int NW(string A, string B, string &alA, string &alB) {
	int * Fprev = new int[B.length() + 1];
	int * Fnew = new int[B.length() + 1];
	bool ** T = new bool*[A.length() + 1];
	bool ** L = new bool*[A.length() + 1];
	int * tmp = NULL;

	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		T[i] = new bool[B.length() + 1];
		L[i] = new bool[B.length() + 1];
		for (unsigned int j = 0; j < B.length() + 1; ++j) {
			T[i][j] = false;
			L[i][j] = false;
		}
		T[i][0] = true;
	}

	for (unsigned int j = 0; j < B.length() + 1; ++j) {
		Fprev[j] = gap() * j; 
		L[0][j] = true;
	}
	int match, del, ins;
	for (unsigned int i = 1; i < A.length() + 1; ++i) {
		Fnew[0] = gap() * i;
		for (unsigned int j = 1; j < B.length() + 1; ++j) {
			match = Fprev[j-1] + S(A[i-1], B[j-1]);
			del = Fprev[j] + gap();
			ins = Fnew[j-1] + gap();
			Fnew[j] = max3(del, match, ins, T[i][j], L[i][j]);
		}

		tmp = Fnew;
		Fnew = Fprev;
		Fprev = tmp;
	}

	unsigned int i = A.length();
	unsigned int j = B.length();
	alA = "";
	alB = "";

	while (i > 0 || j > 0) {
		if (T[i][j]) {
			if (L[i][j]) {
				alA = A[--i] + alA;
				alB = B[--j] + alB;
			} else {
				alA = A[--i] + alA;
				alB = "-" + alB;
			}
		} else {
			alA = "-" + alA;
			alB = B[--j] + alB;
		}
	}
	int res = Fnew[B.length()];
	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		delete [] T[i];
		delete [] L[i];
	}
	delete [] T;
	delete [] L;
	delete [] Fprev;
	delete [] Fnew;

	return res;
}

//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Smith-Waterman Algorithm (not implemented in C++)
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Smith-Waterman-Gotoh Algorithm
int SWG(string A, string B, string &alA, string &alB) {
	int * Dprev = new int[B.length() + 1];
	int * Dnew = new int[B.length() + 1];
	int * P = new int[B.length() + 1];
	bool ** T = new bool*[A.length() + 1];
	bool ** L = new bool*[A.length() + 1];
	bool ** J = new bool*[A.length() + 1];
	int * tmp;

	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		T[i] = new bool[B.length() + 1];
		L[i] = new bool[B.length() + 1];
		J[i] = new bool[B.length() + 1];
		for (unsigned int j = 0; j < B.length() + 1; ++j) {
			T[i][j] = false;
			L[i][j] = false;
			J[i][j] = false;
		}
		T[i][0] = true;
	}
	for (unsigned int j = 0; j < B.length() + 1; ++j) {
		Dprev[j] = 0; 
		P[j] = 0;
		L[0][j] = true;
	}

	int maxv = 0, maxi = 0, maxj = 0;
	int match, Q;
	bool jP = false, jQ = false;
	for (unsigned int i = 1; i < A.length() + 1; ++i) {
		Q = 0;
		Dnew[0] = 0;
		for (unsigned int j = 1; j < B.length() + 1; ++j) {
			P[j] = maxG(Dprev[j] + gap(), P[j], jP);
			Q = maxG(Dnew[j-1] + gap(), Q, jQ);

			match = Dprev[j-1] + S(A[i-1], B[j-1]);
			
			Dnew[j] = max3(P[j], match, Q, T[i][j], L[i][j]);
			if (T[i][j] && !L[i][j]) J[i-1][j] = jP;
			if (!T[i][j] && L[i][j]) J[i][j-1] = jQ;
			if ((i >= A.length() || j >= B.length()) && Dnew[j] > maxv) {
				maxi = i;
				maxj = j;
				maxv = Dnew[j];
			}
		}

		tmp = Dnew;
		Dnew = Dprev;
		Dprev = tmp;
	}

	unsigned int i = maxi;
	unsigned int j = maxj;
	alA = "";
	alB = "";

	if (i < A.length()) {
		alA = A.substr(i);
		for (unsigned int k = i; k < A.length(); ++k) {
			alB += "-";
		}
	}
	if (j < B.length()) {
		for (unsigned int k = j; k < B.length(); ++k) {
			alA += "-";
		}
		alB = B.substr(j);
	}

	bool prevT = true;
	bool prevL = true;
	while (i > 0 && j > 0) {
		if (prevL && !prevT && T[i][j] && L[i][j] && J[i][j]) {
			alA = "-" + alA;
			alB = B[--j] + alB;
		} else if (!prevL && prevT && T[i][j] && L[i][j] && J[i][j]) {
			alA = A[--i] + alA;
			alB = "-" + alB;
		} else if (T[i][j]) {
			if (L[i][j]) {
				alA = A[--i] + alA;
				alB = B[--j] + alB;
				prevT = true;
				prevL = true;
			} else {
				alA = A[--i] + alA;
				alB = "-" + alB;
				prevT = true;
				prevL = false;
			}
		} else {
			alA = "-" + alA;
			alB = B[--j] + alB;
			prevT = false;
			prevL = true;
		}
	}

	if (i > 0) {
		alA = A.substr(0, i) + alA;
		for (unsigned int k = 0; k < i; ++k) {
			alB = "-" + alB;
		}
	}
	if (j > 0) {
		for (unsigned int k = 0; k < j; ++k) {
			alA = "-" + alA;
		}
		alB = B.substr(0, j) + alB;
	}

	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		delete [] T[i];
		delete [] L[i];
		delete [] J[i];
	}
	delete [] T;
	delete [] L;
	delete [] J;
	delete [] Dprev;
	delete [] Dnew;
	delete [] P;

	return maxv;
}

//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Edit distance
int Edit(string A, string B, string &alA, string &alB) {
	int * Fprev = new int[B.length() + 1];
	int * Fnew = new int[B.length() + 1];
	bool ** T = new bool*[A.length() + 1];
	bool ** L = new bool*[A.length() + 1];
	int * tmp;

	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		T[i] = new bool[B.length() + 1];
		L[i] = new bool[B.length() + 1];
		for (unsigned int j = 0; j < B.length() + 1; ++j) {
			T[i][j] = false;
			L[i][j] = false;
		}
		T[i][0] = true;
	}
	for (unsigned int j = 0; j < B.length() + 1; ++j) {
		Fprev[j] = j; 
		L[0][j] = true;
	}

	int match, del, ins, res = 0;
	for (unsigned int i = 1; i < A.length() + 1; ++i) {
		Fnew[0] = i;
		for (unsigned int j = 1; j < B.length() + 1; ++j) {
			if (S(A[i-1], B[j-1]) > 0) {
				Fnew[j] = Fprev[j-1];
				T[i][j] = true;
				L[i][j] = true;
			} else {
				match = Fprev[j-1] + 1;
				del = Fprev[j] + 1;
				ins = Fnew[j-1] + 1;
				Fnew[j] = min3(del, match, ins, T[i][j], L[i][j]);
			}
		}
		tmp = Fnew;
		Fnew = Fprev;
		Fprev = tmp;
	}
	res = Fprev[B.length()];

	int i = A.length();
	int j = B.length();
	alA = "";
	alB = "";

	while (i > 0 || j > 0) {
		if (T[i][j]) {
			if (L[i][j]) {
				alA = A[--i] + alA;
				alB = B[--j] + alB;
			} else {
				alA = A[--i] + alA;
				alB = "-" + alB;
			}
		} else {
			alA = "-" + alA;
			alB = B[--j] + alB;
		}
	}

	for (unsigned int i = 0; i < A.length() + 1; ++i) {
		delete [] T[i];
		delete [] L[i];
	}
	delete [] T;
	delete [] L;
	delete [] Fprev;
	delete [] Fnew;

	return ((A.length() + B.length())  - (2*res)) / 2;
}

//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
//Main
int main(int argc, char* argv[]) {
	if (argc < 3 || argc > 4) {
		cout << "Wrong usage! Use " << argv[0] << " bases.fasta predictions.fasta [method (local, global, edit)]" << endl;
		return 1;
	}

	string bFile(argv[1]);
	string pFile(argv[2]);
	string oFile(pFile.substr(0, pFile.find(".fasta")) + "_Align");

	string method;
	if (argc == 3) method = "edit";
	else method = string(argv[3]);
	cout << "Aligning " << method << endl;
	if (method == "local") oFile += "Local.fasta";
	else if (method == "global") oFile += "Global.fasta";
	else if (method == "edit") oFile += "Edit.fasta";
	else {
		cout << "No method: " << method << "! Available methods: local, global, edit" << endl;
		return 2;
	}

	ifstream inB;
	inB.open(bFile.c_str());
	
	ifstream inP;
	inP.open(pFile.c_str());
	
	ofstream out;
	out.open(oFile.c_str());
	
	string labelB, labelP;
	string basesB, basesP;
	string B, P;
	int res;
	double rat;

	while(true) {
		inB >> labelB >> basesB;
		inP >> labelP >> basesP;
		if (!inB.good() || !inP.good())	break;
		
		if (labelP != labelB) {
			cout << "Warning: aligning different sequences! Check input files." << endl;
		}

		if (method == "local") res = SWG(basesB, basesP, B, P);
		else if (method == "global") res = NW(basesB, basesP, B, P);
		else res = Edit(basesB, basesP, B, P);
		
		rat = 2 * res;
		rat /= (basesB.length() + basesP.length());
		out << labelB << "_" << res << "_" << rat << endl;
		out << B << endl;
		out << P << endl;
	}
	return 0;
}	
