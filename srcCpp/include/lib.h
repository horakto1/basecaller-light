#ifndef LIBKJLASJFLAJSFLSKFASLKJDLQ
#define LIBKJLASJFLAJSFLSKFASLKJDLQ

#include <math.h>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <cfloat>
#include <algorithm>

using namespace std;

//----------------------------------------------------------------------------------------------------//
// Conversions
int bToi(char base);
char iTob(int i);
int sToi(string s);
string iTos(int i, int l = 3);
//-----------------------------------------------------------------------------------------------------//
// Class Table
class Table {
public:
	vector<vector<double> > matrix;
	Table(int move, int l);
	double get(int prev, int next);
	void print();
};
//----------------------------------------------------------------------------------------------------//
// Reading files

// Reads means and standard devitaions of currents for each base l-mer from csv file
vector<pair<double, double> > readTable(string f, int l = 3);
//----------------------------------------------------------------------------------------------------//
// Handling results

// Translating sequence of states to a sequence
string consolidate(vector<unsigned int> seq, int l = 3);
// Optional consolidation for Forward-Backward method
string consolidateFB(vector<unsigned int> seq, int l = 3);
//----------------------------------------------------------------------------------------------------//
// Transitions

// Transition probability between prev and cur contexts
double getTransP(vector<Table *> * matrices, int prev, int cur, double stay_prob, double step_prob, double skip_prob);
// Logaritmized Transition probability
double getTransPLog(vector<Table *> * matrices, int prev, int cur, double stay_prob, double step_prob, double skip_prob);
// Returns whether prev context is followed by new context using move moves
bool follows(int prev, int next, int move, int l);
// Returns the minimal value of move between prev and st contexts
int followsN(int prev, int st, int l);
// Creates an array of matrices with transition probabilities for specific move value
vector<Table *> createTM(int l = 3);
bool notIn(vector<unsigned int> vec, unsigned int value);
// Returns an array of followers for each context
vector<vector<unsigned int> > findFollowers(int l);
// Returns an array of predecessors for each context
vector<vector<unsigned int> > findPredecessors(int l);
//----------------------------------------------------------------------------------------------------//
// Math
double expX(double x);
double logX(double x);
double normVal(double x, double m, double s);

#endif
