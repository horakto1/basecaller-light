#!/usr/bin/python

import sys
import random
import csv
import lib
import matplotlib.pyplot as plt
from scipy.stats import norm	# Gauss distribution for probabilities


# Generates a sequence of Bases using uniform distribution
def genBases(minLen, maxLen):
	l = random.randint(minLen, maxLen)

	seq = []
	for i in range(0,l):
		x = random.randint(0, 3)

		if (x == 0):
			seq.append('A')
		elif (x == 1):
			seq.append('C')
		elif (x == 2):
			seq.append('G')
		elif (x == 3):
			seq.append('T')

	return seq

# Base on the sequence a settings generates currents (events) using Gaussian distribution
def genCurrent(bases, settings, table):
	seq = []
	i = settings['size']
	moves = [0]
	while (True):
		if (i >= len(bases)):
			break
		st = bases[i - settings['size']:i]
		intSt = lib.sToi(st)
		val = table[intSt][0]
		diff = random.gauss(0, table[intSt][1])
		seq.append(int(round(val + diff)))

		move = random.random() - settings['stay_prob']
		moves.append(0)
		if (move > 0):
			i+=1
			moves[-1] += 1
			move -= settings['step_prob']
			if (move > 0):
				i+=1
				moves[-1] += 1

	return seq, moves


def main(minLen, maxLen, n, size, stay_prob, skip_prob, tableFile, out):
	settings = {}
	settings['size'] = size
	settings['stay_prob'] = stay_prob
	settings['skip_prob'] = skip_prob
	settings['step_prob'] = 1 - settings['stay_prob'] - settings['skip_prob']

	with open(tableFile, 'r') as f:
		table = lib.readTable(f, settings['size'])

	realAvg = 0
	with open(out + 'Bases' + str(settings['size']) + '.fasta', 'w') as fb, open(out + 'Currents' + str(settings['size']) + '.fasta', 'w') as fc:
		for i in range(0, n):
			bases = genBases(minLen, maxLen)
			current, moves = genCurrent(bases, settings, table)
			realAvg += (len(current) / len(bases))

			basesStr = ''
			for b in bases:
				basesStr += b

			currStr = ''
			for c in current:
				currStr += (str(c) + ',')

			fb.write('>n' + str(i) + '_size' + str(settings['size']) + '_len' + str(len(bases)) + '\n')
			fb.write(basesStr + '\n')

			fc.write('>n' + str(i) + '_size' + str(settings['size']) + '_len' + str(len(bases)) + '\n')
			fc.write(currStr[:-1] + '\n')


if __name__ == "__main__":
	if (len(sys.argv) < 9 or len(sys.argv) > 9):
		print('Wrong usage! Use script.py minLen maxLen count size stay_prob skip_prob table.csv out')
		exit(0);

	main(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), sys.argv[7], sys.argv[8])



