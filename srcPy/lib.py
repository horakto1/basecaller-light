import csv
import numpy as np
import h5py

#----------------------------------------------------------------------------------------------------#
# Conversions

def bToi(base):
	if (base == 'A'):
		return 0
	elif (base == 'C'):
		return 1
	elif (base == 'G'):
		return 2
	elif (base == 'T'):
		return 3

def iTob(i):
	if (i == 0):
		return 'A'
	elif (i == 1):
		return 'C'
	elif (i == 2):
		return 'G'
	elif (i == 3):
		return 'T'

def sToi(s):
	if (len(s) == 1):
		return bToi(s[0])
	return bToi(s[-1]) + (4 * sToi(s[:-1]))

def iTos(i, l = 3):
	if (l == 0):
		return []
	return iTos(int(i / 4), l - 1) + [iTob(i % 4)]

#----------------------------------------------------------------------------------------------------#
# Reading files

# Reads means and standard devitaions of currents for each base l-mer from csv file
def readTable(f, l = 3):
	table = []
	for i in range(4 ** l):
		table.append([0, 0])
	reader = csv.reader(f, delimiter=',', quotechar='"')
	for r in reader:
		table[sToi(r[0:l])] = [float(r[-2]), float(r[-1])]
	return table

# UNUSED Reads probabilities of mean currents for each base triplet from csv file
def readProps(f):
	props = []
	reader = csv.reader(f, delimiter=',', quotechar='"')
	return [float(r[1]) for r in reader]

# Reads actual currents reported by nanopore for one record
def readCurrents(f):
	label = f.readline()
	if (not label or label == ''):
		return '', []
	return label[:-1], [float(c) for c in f.readline().split(',')]

# UNUSED
def readSettings(file):
	settings = {}
	reader = csv.reader(file, delimiter=',', quotechar='"')
	for r in reader:
		if (r[0] in ['SD', 'len', 'stay_prob', 'step_prob', 'skip_prob']):
			settings[r[0]] = float(r[1])
		elif (r[0]) in ['size']:
			settings[r[0]] = int(r[1])
		else:
			settings[r[0]] = r[1]
	return settings

#----------------------------------------------------------------------------------------------------#
# Handling results

# Translating sequence of states to a sequence
def consolidate(seq, l = 3):
	res = []
	moves = [0]
	prev = seq[0]
	s = iTos(seq[0], l)
	res2 = [s]
	for i in range(l):
		res.append(s[i])

	for t in range(1, len(seq)):
		s = iTos(seq[t], l)
		res2.append(s)

		if (prev == seq[t]):
			moves.append(0)
		else:
			m1 = followsN(prev, seq[t], l)
			moves.append(m1)
			for j in range(-m1, 0, 1):
				res.append(s[j])
			
		prev = seq[t]
	return res, moves

# Optional consolidation for Forward-Backward method
def consolidateFB(seq, l = 3):
	res = []
	moves = [0]
	prev = seq[0]
	s = iTos(seq[0], l)
	res2 = [s]
	for i in range(l):
		res.append(s[i])

	for t in range(1, len(seq) - 1):
		s = iTos(seq[t], l)
		sNext = iTos(seq[t+1], l)
		res2.append(s)

		if (prev == seq[t]):
			moves.append(0)
		else:
			m1 = followsN(prev, seq[t], l)
			if (m1 >= 3 and followsN(seq[t], seq[t+1], l) >= 3):
				moves.append(-1)
				m2 = followsN(prev, seq[t+1], l)
				moves.append(m2)
				for j in range(-m2, 0, 1):
					res.append(sNext[j])
				t += 1
			else:
				moves.append(m1)
				for j in range(-m1, 0, 1):
					res.append(s[j])
		
		prev = seq[t]

	s = iTos(seq[-1], l)
	res2.append(s)
	m1 = followsN(prev, seq[-1], l)
	moves.append(m1)
	for j in range(-m1, 0, 1):
		res.append(s[j])
		
	return res, moves

# UNUSED - Only for patching purposes
def checkMoves(label, moves):
	f = h5py.File('data/fast5/' + label[1:], 'r')
	events = f.get('Analyses').get('Basecall_1D_000').get('BaseCalled_template').get('Events')
	
	print(len(events), len(moves))

	diff = []
	m = []
	m2 = [0] * 11
	for i, e in enumerate(events):
		#print(e[5], moves[i])
		#asd = raw_input()
		if (e[5] != moves[i]):
			diff.append(i)
			m.append(e[5] - moves[i])
			m2[e[5] - moves[i] + 5] += 1
		else:
			m2[5] += 1

	#print(diff)
	#print(','.join(map(str,m)))
	print(m2)
	#print(len(diff), len(moves))

#----------------------------------------------------------------------------------------------------#
# Transitions

# Transition probability between prev and cur contexts
def getTransP(matrices, settings, prev, cur):
	p = 0
	for i in range(len(settings)):
		p += matrices[i][prev][cur] * settings[i]
	return p

# Logaritmized Transition probability
def getTransPLog(matrices, settings, prev, cur):
	return log(getTransP(matrices, settings, prev, cur))

# Creates an array of move probabilities
def getMoveProbs(settings):
	l = settings['size']
	probs = [settings['stay_prob']]
	probs.append(1 - settings['stay_prob'] - settings['skip_prob'])
	probs.append(settings['skip_prob'])
	return probs

# Returns whether prev context is followed by new context using move moves
def follows(prev, new, move, l):
	d = 4 ** move
	m = 4 ** (l - move)
	return (prev % m) == int(new / d)

# Returns the minimal value of move between prev and st contexts
def followsN(prev, st, l):
	if (prev == st):
		return 0

	for i in range(1,l):
		if (follows(prev, st, i, l)):
			return i
	return l

# Creates an array of matrices with transition probabilities for specific move value
def createTM(l = 3):
	matrices = []

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for i in range(4 ** l):
		matrix[i][i] = 1.0
	matrices.append(matrix)

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for prev in range(4 ** l):
		for new in range(4 ** l):
			if (follows(prev, new, 1, l)):
				matrix[prev][new] = 0.25
	matrices.append(matrix)

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for prev in range(4 ** l):
		for new in range(4 ** l):
			if (follows(prev, new, 2, l)):
				matrix[prev][new] = 0.0625
	matrices.append(matrix)

	return matrices

# UNUSED - Adds end state to all Matrices (for Forward-Backward algorithm)
def addEnd(matrices):
	end = 1.0 / 9000
	end1 = 1 - end

	for m in range(len(matrices)):
		for i in range(len(matrices[m])):
			for j in range(len(matrices[m][i])):
				matrices[m][i][j] *= end1
			matrices[m][i].append(end)
		matrices[m].append([0] * len(matrices[m][0]))
		matrices[m][-1][-1] = 1
								
	return matrices

# UNUSED - Logaritmizes all tables
def logMatrix(matrices):
	for m in range(len(matrices)):
		for i in range(len(matrices[m])):
			for j in range(len(matrices[m][i])):
				if (matrices[m][i][j] <= 0):
					matrices[m][i][j] = float('-inf');
				else:
					matrices[m][i][j] = np.log(matrices[m][i][j])
	return matrices

# Returns an array of followers for each context
def findFollowers(l):
	followers = []
	for i in range(4 ** l):
		followers.append([])

		moved = (i % (4 ** (l - 2))) * 16
		for j in range(16):
			followers[i].append(moved + j)
		moved = (i % (4 ** (l - 1))) * 4
		for j in range(4):
			if (moved + j not in followers[i]):
				followers[i].append(moved + j)
		if (i not in followers[i]):
			followers[i].append(i)
		
	return followers

# Returns an array of predecessors for each context
def findPredecessors(l):
	pred = []
	pred2 = []
	for i in range(4 ** l):
		pred2.append([])
		pred.append([])

		moved = int(i / 16)
		for j in range(16):
			pred[i].append(moved + (j * (4 ** (l - 2))))
		moved = int(i / 4)
		for j in range(4):
			if (moved + (j * (4 ** (l - 1))) not in pred[i]):
				pred[i].append(moved + (j * (4 ** (l - 1))))
		if (i not in pred[i]):
			pred[i].append(i)
		
	return pred

#----------------------------------------------------------------------------------------------------#
# Math

# Simplified exp method with limit
def exp(x):
	if (x == float('-inf')):
		return 0
	return np.exp(x)

# Simplified log method with limit
def log(x):
	if (x <= 0):
		return float('-inf')
	return np.log(x)
