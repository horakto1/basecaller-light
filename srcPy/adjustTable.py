#!/usr/bin/python

from lib import *
import sys

# Key to sort based on first value
def sort1(a):
	return a[0]

# Key to sort based on second value
def sort2(a):
	return a[1]

# Found percentiles of the first contexts
def percStart(l):
	if (l == 3):
		return 0.032041
	if (l == 4):
		return 0.005849
	return 0.003904

# Found percentiles of the last contexts
def percEnd(l):
	if (l == 3):
		return 0.968985
	if (l == 4):
		return 0.988762
	return 0.994136
	
def main(l, tFile, cFile, outFile):
	with open(tFile, 'r') as t:
		tableX = readTable(t, l)
	table = []
	for t in range(len(tableX)):
		table.append([t] + tableX[t])

	cur = []
	with open(cFile, 'r') as currents:
		while(True):
			label, c = readCurrents(currents)
			if (label == ''):
				break
			cur += c

	table = sorted(table, key=sort2)

	print("Default Table:")
	print(iTos(table[0][0], l), table[0][0], table[0][1])
	print(iTos(table[-1][0], l), table[-1][0], table[-1][1])
	cur = sorted(cur)

	first = cur[int(percStart(l) * len(cur))]
	last = cur[int(percEnd(l) * len(cur))]

	newDiff = last - first
	oldDiff = table[-1][1] - table[0][1]
	ratio = newDiff / oldDiff
	new = []
	for t in range(len(table)):
		newMu = (table[t][1] - table[0][1]) * ratio + first
		newSd = table[t][2] * ratio
		new.append([table[t][0], newMu, newSd])

	print("New Table:")
	print(iTos(new[0][0], l), new[0][0], new[0][1])
	print(iTos(new[-1][0], l), new[-1][0], new[-1][1])
	new = sorted(new, key=sort1)

	with open(outFile, 'w') as out:
		writer = csv.writer(out, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
		for t in new:
			s = iTos(t[0], l)
			writer.writerow(s + [t[1], t[2]])

if __name__ == "__main__":
	if (len(sys.argv) < 5 or len(sys.argv) > 5):
		print('Wrong usage! Use script.py size defaultTable.csv currents.fasta outTable.csv')
		exit(0)
	main(int(sys.argv[1]), sys.argv[2], sys.argv[3], sys.argv[4])
