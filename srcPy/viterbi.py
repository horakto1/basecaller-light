#!/usr/bin/python

# Viterbi Algorithm

import sys						# managing argv
from scipy.stats import norm	# Gauss distribution for probabilities
from lib import *				# functions used in both viterbi and fwd-bkw

#----------------------------------------------------------------------------------------------------#
# Viterbi
def viterbi(currents, transM, emMeans, settings, pred):
	l = settings['size']
	moveProbs = getMoveProbs(settings)
	
	probs = [[]]
	prev = [[]]

	# First Event
	for st in range(len(emMeans)):
		probs[0].append(log(norm.pdf(currents[0], emMeans[st][0], emMeans[st][1])))
		prev[0].append(None)

	#print(len(currents))
	# Core phase
	for t in range(1, len(currents)):
		sys.stdout.write('\r' + str(t))
		sys.stdout.flush()
		
		probs.append([])
		prev.append([])
			
		maxForCur = float('-inf')
		for st in range(len(emMeans)):
			pos = 0
			maxProb = probs[t-1][pred[st][0]] + getTransPLog(transM, moveProbs, pred[st][0], st) 
			for prevSt in pred[st][1:]:
				tmp = probs[t-1][prevSt] + getTransPLog(transM, moveProbs, prevSt, st) 
				if (tmp > maxProb):
					maxProb = tmp
					pos = prevSt

			maxProb += log(norm.pdf(currents[t], emMeans[st][0], emMeans[st][1]))
			if (maxProb >= maxForCur):
				maxForCur = maxProb

			probs[t].append(maxProb)
			prev[t].append(pos)

		for i in range(len(probs[t])):
			probs[t][i] -= maxForCur

	# Position of backtrack start
	previous = 0
	mmax = probs[-1][0]
	for i in range(1, len(probs[-1][1:])):
		if (probs[-1][i] > mmax):
			mmax = probs[-1][i]
			previous = i
	opt = [previous]
	
	# Backtrack Phase
	for t in range(len(probs) - 2, -1, -1):
		opt.insert(0, prev[t+1][previous])
		previous = prev[t+1][previous]

	return opt, maxProb

#----------------------------------------------------------------------------------------------------#
# Main
def main(cFile, tFile, outFile, sets):
	settings = {}
	settings['size'] = int(sets[0])
	settings['stay_prob'] = float(sets[1])
	settings['skip_prob'] = float(sets[2])
	settings['step_prob'] = 1 - (settings['stay_prob'] + settings['skip_prob'])

	with open(tFile, 'r') as f:
		emMeans = readTable(f, settings['size'])

	pred = findPredecessors(settings['size'])
	transM = createTM(settings['size'])
	with open(cFile, 'r') as f, open(outFile, 'w') as out:
		while(True):
			label, cur = readCurrents(f)
			if (label == ''):
				break
			print(label)
			out.write(label + "\n")
			res, p = viterbi(cur, transM, emMeans, settings, pred)
			res2, moves = consolidate(res, settings['size'])
			#print(len(res), len(res2))
			#checkMoves(label, moves)
			r = ""
			for x in res2:
				r += x
			out.write(r + "\n")



if __name__ == "__main__":
	if (len(sys.argv) < 7 or len(sys.argv) > 7):
		print('Wrong usage! Use script.py currents.fasta table.csv output.fasta size stay_prob skip_prob')
		exit(0);

	main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4:])
