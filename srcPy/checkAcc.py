#!/usr/bin/python

import sys
from math import sqrt
from decimal import Decimal

# Reads the outputs of Align(.py) and writes consolidated statistics add the end of out file
def main(start, out):
	start = start.split('.fasta')[0]
	loc = start + '_AlignLocal.fasta'
	glob = start + '_AlignGlobal.fasta'
	edit = start + '_AlignEdit.fasta'
	
	gAvg = 0
	lAvg = 0
	eAvg = 0

	gMin = 1
	lMin = 1
	eMin = 1

	gMax = 0
	lMax = 0
	eMax = 0

	gCs = []
	lCs = []
	eCs = []

	n = 0
	with open(loc, 'r') as l, open(glob, 'r') as g, open(edit, 'r') as e, open(out, 'a') as o:
		while(True):
			labelL = l.readline()[:-1].split('_')
			l.readline()
			l.readline()

			labelG = g.readline()[:-1].split('_')
			g.readline()
			g.readline()

			labelE = e.readline()[:-1].split('_')
			e.readline()
			e.readline()

			if (labelG == [''] or labelL == [''] or labelE == ['']):
				break
			
			n += 1
			gC = float(labelG[-1])
			lC = float(labelL[-1])
			eC = float(labelE[-1])

			gCs.append(gC)
			lCs.append(lC)
			eCs.append(eC)

			gAvg += gC
			lAvg += lC
			eAvg += eC

			if (lC < lMin):
				lMin = lC
			if (gC < gMin):
				gMin = gC
			if (eC < eMin):
				eMin = eC

			if (lC > lMax):
				lMax = lC
			if (gC > gMax):
				gMax = gC
			if (eC > eMax):
				eMax = eC
					
		gAvg = gAvg / n
		lAvg = lAvg / n
		eAvg = eAvg / n

		gSdS = 0
		lSdS = 0
		eSdS = 0

		for i in range(len(gCs)):
			gSdS += (gCs[i] - gAvg) * (gCs[i] - gAvg)
			lSdS += (lCs[i] - lAvg) * (lCs[i] - lAvg)
			eSdS += (eCs[i] - eAvg) * (eCs[i] - eAvg)

		gSd = sqrt(gSdS / n)
		lSd = sqrt(lSdS / n)
		eSd = sqrt(eSdS / n)

		o.write(start + '\n')
		o.write('Local  Min:\t' + str(lMin) + '\tAvg: ' + str(lAvg) + '\tMax: ' + str(lMax) + '\tSd: ' + str(lSd) + '\n')
		o.write('Global Min:\t' + str(gMin) + '\tAvg: ' + str(gAvg) + '\tMax: ' + str(lMax) + '\tSd: ' + str(gSd) + '\n')
		o.write('Edit   Min:\t' + str(eMin) + '\tAvg: ' + str(eAvg) + '\tMax: ' + str(eMax) + '\tSd: ' + str(eSd) + '\n\n')

if __name__ == "__main__":
	if (len(sys.argv) < 3 or len(sys.argv) > 3):
		print('Wrong usage! Use script.py fileName outFile')
		exit(0);
	
	main(sys.argv[1], sys.argv[2])
