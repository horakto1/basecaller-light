#!/usr/bin/python
import sys						# managing argv
import copy 					# deepcopy of vectors
from bitarray import bitarray	# directions kept in bitarrays

# Symbol comaparison (replaces diagonal matrix with 1s on diagonal, -1s otherwise)
def S(a, b):
	if (a == b):
		return 1
	return -1

# Gap Penalty
def gap():
	return -1

# For SW,NW returns max result for insert, delete and substitution with the direction in 2 bits
def max3(t, tl, l):
	T = False
	L = False
	m = 0
	if (l >= m):
		m = l
		L = True
	if (t >= m):
		m = t
		T = True
		L = False
	if (tl >= m):
		m = tl
		T = True
		L = True
	return m, T, L

# For Edit distance same as max3 only returns minimum
def min3(t, tl, l):
	T = True
	L = True
	m = tl
	if (t < m):
		m = t
		L = False
	if (l < m):
		m = l
		T = False
		L = True
	return m, T, L

# Gotoh improvement may follow in previous direction 
def maxG(noJump, jump):
	if (noJump > jump):
		return noJump, False
	return jump, True

#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#
# Needleman-Wunsh Algorithm
def NW(A, B):
	Fprev = [None] * (len(B) + 1)
	Fnew = [None] * (len(B) + 1)
	T = []
	L = []

	for i in range(0, len(A) + 1):
		T.append(bitarray(len(B) + 1))
		L.append(bitarray(len(B) + 1))
		T[i].setall(False)
		L[i].setall(False)
		T[i][0] = True

	for j in range(0, len(B) + 1):
		Fprev[j] = gap() * j 
		L[0][j] = True
		
	for i in range(1, len(A) + 1):
		Fnew[0] = gap() * i
		for j in range(1, len(B) + 1):
			match = Fprev[j-1] + S(A[i-1], B[j-1])
			delete = Fprev[j] + gap()
			insert = Fnew[j-1] + gap()
			Fnew[j], T[i][j], L[i][j] = max3(delete, match, insert)

		Fprev = copy.deepcopy(Fnew)

	i = len(A)
	j = len(B)
	AlignmentA = ''
	AlignmentB = ''

	while (i > 0 or j > 0):
		if (T[i][j]):
			if (L[i][j]):
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = B[j-1] + AlignmentB
				i -= 1
				j -= 1
			else:
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = '-' + AlignmentB
				i -= 1
		else:
			AlignmentA = '-' + AlignmentA
			AlignmentB = B[j-1] + AlignmentB
			j -= 1

	return Fnew[len(B)], AlignmentA, AlignmentB

#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#
# Smith-Watermann Algorithm
def SW(A, B):
	F = [ [None] * (len(B) + 1) for _ in range(len(A) + 1) ]
	D = [ [None] * (len(B) + 1) for _ in range(len(A) + 1) ]
	T = []
	L = []

	for i in range(0, len(A) + 1):
		F[i][0] = 0
		T.append(bitarray(len(B) + 1))
		L.append(bitarray(len(B) + 1))
		T[i].setall(False)
		L[i].setall(False)
		T[i][0] = True

	for j in range(0, len(B) + 1):
		F[0][j] = 0
		L[0][j] = True
		
	maxv = 0
	maxi = 0
	maxj = 0
	for i in range(1, len(A) + 1):
		for j in range(1, len(B) + 1):
			match = F[i-1][j-1] + S(A[i-1], B[j-1])
			delete = max([x[j] for x in F[:i]]) + gap()
			insert = max([x for x in F[i][:j]]) + gap()
			F[i][j], T[i][j], L[i][j] = max3(delete, match, insert)

			if ((i >= len(A) or j >= len(B)) and F[i][j] > maxv):
				maxi = i
				maxj = j
				maxv = F[i][j]


	i = len(A)
	j = len(B)
	AlignmentA = ''
	AlignmentB = ''

	if (i < len(A)):
		AlignmentA += A[i:]
		AlignmentB += ('-' * len(A[i:]))
	if (j < len(B)):
		AlignmentA += ('-' * len(B[j:]))
		AlignmentB += B[j:]

	while (i > 0 and j > 0):
		if (T[i][j]):
			if (L[i][j]):
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = B[j-1] + AlignmentB
				i -= 1
				j -= 1
			else:
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = '-' + AlignmentB
				i -= 1
		else:
			AlignmentA = '-' + AlignmentA
			AlignmentB = B[j-1] + AlignmentB
			j -= 1

	if (i > 0):
		AlignmentA = A[:i] + AlignmentA
		AlignmentB = ('-' * len(A[:i])) + AlignmentB
	if (j > 0):
		AlignmentA = ('-' * len(B[:j])) + AlignmentA
		AlignmentB = B[:j] + AlignmentB


	return maxv, AlignmentA, AlignmentB

#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#
# Smith-Watermann-Gotoh Algorithm
def SWG(A, B):
	Dprev = [0] * (len(B) + 1)
	Dnew = [None] * (len(B) + 1)
	Dprev[0] = 0
	
	P = [0] * (len(B) + 1)
	
	T = []
	L = []
	J = []
	
	for i in range(0, len(A) + 1):
		T.append(bitarray(len(B) + 1))
		L.append(bitarray(len(B) + 1))
		T[i].setall(False)
		L[i].setall(False)
		T[i][0] = True
		
		J.append(bitarray(len(B) + 1))
		J[i].setall(False)
		

	for j in range(0, len(B) + 1):
		L[0][j] = True
		
	maxv = 0
	maxi = 0
	maxj = 0
	for i in range(1, len(A) + 1):
		Q = 0
		Dnew[0] = 0

		for j in range(1, len(B) + 1):

			P[j], jP = maxG(Dprev[j] + gap(), P[j])
			Q, jQ = maxG(Dnew[j-1] + gap(), Q)

			match = Dprev[j-1] + S(A[i-1], B[j-1])
			
			Dnew[j], T[i][j], L[i][j] = max3(P[j], match, Q)
			if (T[i][j] and not L[i][j]):
				J[i-1][j] = jP
			if (not T[i][j] and L[i][j]):
				J[i][j-1] = jQ

			if ((i >= len(A) or j >= len(B)) and Dnew[j] > maxv):
				maxi = i
				maxj = j
				maxv = Dnew[j]

		Dprev = copy.deepcopy(Dnew)

	i = maxi
	j = maxj
	AlignmentA = ''
	AlignmentB = ''

	if (i < len(A)):
		AlignmentA += A[i:]
		AlignmentB += ('-' * len(A[i:]))
	if (j < len(B)):
		AlignmentA += ('-' * len(B[j:]))
		AlignmentB += B[j:]

	prevT = True
	prevL = True
	while (i > 0 and j > 0):
		if (prevL and not prevT and T[i][j] and L[i][j] and J[i][j]):
			AlignmentA = '-' + AlignmentA
			AlignmentB = B[j-1] + AlignmentB
			j -= 1
		elif (not prevL and prevT and T[i][j] and L[i][j] and J[i][j]):
			AlignmentA = A[i-1] + AlignmentA
			AlignmentB = '-' + AlignmentB
			i -= 1
		elif (T[i][j]):
			if (L[i][j]):
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = B[j-1] + AlignmentB
				i -= 1
				j -= 1
				prevT = True
				prevL = True
			else:
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = '-' + AlignmentB
				i -= 1
				prevT = True
				prevL = False
		else:
			AlignmentA = '-' + AlignmentA
			AlignmentB = B[j-1] + AlignmentB
			j -= 1
			prevT = False
			prevL = True
	
	if (i > 0):
		AlignmentA = A[:i] + AlignmentA
		AlignmentB = ('-' * len(A[:i])) + AlignmentB
	if (j > 0):
		AlignmentA = ('-' * len(B[:j])) + AlignmentA
		AlignmentB = B[:j] + AlignmentB

	return maxv, AlignmentA, AlignmentB

#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#
# Edit distance
def Edit(A, B):
	Fprev = [0] * (len(B) + 1)
	Fnew = [None] * (len(B) + 1)
	T = []
	L = []

	for i in range(0, len(A) + 1):
		T.append(bitarray(len(B) + 1))
		L.append(bitarray(len(B) + 1))
		T[i].setall(False)
		L[i].setall(False)
		T[i][0] = True

	for j in range(0, len(B) + 1):
		Fprev[j] = j
		L[0][j] = True
		
	for i in range(1, len(A) + 1):
		Fnew[0] = i
		for j in range(1, len(B) + 1):
			if (S(A[i-1], B[j-1]) > 0):
				Fnew[j], T[i][j], L[i][j] = Fprev[j-1], True, True
			else:
				match = Fprev[j-1] + 1
				delete = Fprev[j] + 1
				insert = Fnew[j-1] + 1
				Fnew[j], T[i][j], L[i][j] = min3(delete, match, insert)

		Fprev = copy.deepcopy(Fnew)

	i = len(A)
	j = len(B)
	AlignmentA = ''
	AlignmentB = ''

	while (i > 0 or j > 0):
		if (T[i][j]):
			if (L[i][j]):
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = B[j-1] + AlignmentB
				i -= 1
				j -= 1
			else:
				AlignmentA = A[i-1] + AlignmentA
				AlignmentB = '-' + AlignmentB
				i -= 1
		else:
			AlignmentA = '-' + AlignmentA
			AlignmentB = B[j-1] + AlignmentB
			j -= 1

	return ((len(A) + len(B)) - (2*Fnew[len(B)])) / 2, AlignmentA, AlignmentB

#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#----------#
#Main
def main(basesFile, predictionsFile, method = 'global'):
	outFile = predictionsFile.split('.fasta')[0] + '_Align'

	if (method == 'local'):
		outFile += 'Local.fasta'
	elif (method == 'global'):
		outFile += 'Global.fasta'
	elif (method == 'edit'):
		outFile += 'Edit.fasta'
	elif (method == 'slow'):
		outFile += 'LocalSlow.fasta'
	else:
		print('No method: "' + method + '"! Available methods: local, global, edit, (slow)')
		exit(0);

	with open(basesFile, 'r') as b, open(predictionsFile, 'r') as p, open(outFile, 'w') as out:
		while(True):
			labelB = b.readline()[:-1]
			seq = b.readline()[:-1]

			labelP = p.readline()[:-1]
			seqP = p.readline()[:-1]
			
			if (labelB == '' or labelP == ''):
				break
			if (labelP != labelB):
				print("Warning: aligning different sequences! Check input files.")
			
			if (method == 'local'):
				res, A, B = SWG(seq, seqP)
			elif (method == 'global'):
				res, A, B = NW(seq, seqP)			
			elif (method == 'edit'):
				res, A, B = Edit(seq, seqP)			
			else:
				res, A, B = SW(seq, seqP)	
			
			out.write(labelB + "_" + str(res) + "_" + str(float(2*res) / (len(seqP) + len(seq))) + "\n")
			
			out.write(A + '\n')
			out.write(B + '\n')


if __name__ == "__main__":
	if (len(sys.argv) < 3 or len(sys.argv) > 4):
		print('Wrong usage! Use script.py bases.fasta out.fasta [method (local, global, slow, edit)]')
		exit(0)

	if (len(sys.argv) == 3):
		main(sys.argv[1], sys.argv[2])
	else:
		main(sys.argv[1], sys.argv[2], sys.argv[3])
