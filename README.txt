README of Master's Thesis by Horak Tomas, FIT CTU in Prague

Content of README
    1. General Information..........line 21
    2. Content drive/repository.....line 39 
    3. Installation.................line 98
        a. C++
        b. Python
    4. Usage........................line 120
        a. adjustTable
        b. Viterbi
        c. Forward-Backward
        d. Align
        e. checkAcc
        f. generateGrouped

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

1. General Information

Supervisor: Mgr. Petr Danecek, Ph.D.
Author: Tomas Horak, FIT CTU in Prague, Knowledge Engineering
Contact: horakto1@gmail.com
License: Open-Source project, so feel free to use or modify

A base-calling tool using Viterbi or Forward-Backward Aglorithm.
Several supporting scripts are provided including alignment tool
and a way to scale parameters for the input data.

Provided are also generated data and script which can generate more.
Two sample sets of real E.coli data sequenced by R9 chemistry are included as well.

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

2. Content of the enclosed drive / repository

.data                                 # Testing data
    .generated                        # Generated sequences and currents based on the tables
        - bases3.fasta
        - bases4.fasta
        - bases5.fasta
        - currents3.fasta
        - currents4.fasta
        - currents5.fasta
        - table3.fasta
        - table4.fasta
        - table5.fasta
    .sample1        
        .fast5                        # Original fast5 files of sample1 sequences
        - bases.fasta
        - currents.fasta
        - table3.fasta
        - table4.fasta
        - table5.fasta
    .sample2
        .fast5                        # Original fast5 files of sample2 sequences
        - bases.fasta
        - currents.fasta
        - table3.fasta
        - table4.fasta
        - table5.fasta
.exe
    - viterbi
    - fwd_bkw
    - align
.srcCpp
    .include                          # Header files
        - align.h
        - fwd_bkw.h
        - lib.h
        - viterbi.h
    .src                              # .cpp files
        - align.cpp
        - fwd_bkw.cpp
        - lib.cpp
        - viterbi.cpp
    - Makefile                        # Makefile compiles all C++ source codes 
.srcPy
    - adjustTable.py                  # Scaling of paremeters
    - align.py
    - checkAcc.py                     # Consolidates all alignments
    - fwd_bkw.py
    - generateGrouped.py              # Scripts used for generating dummy data
    - lib.py
    - viterbi.py
.srcThesis                            # Latex source files of this Thesis
- MT_Horak_Tomas_2017.pdf             # Final version of Thesis text
- README.txt                          # This readme

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

3. Installation

    a. C++

Makefile is provided so in UNIX environment use command make in srcCpp folder.

If not using Makefile, used flags are:
    std=c++11
    O3                                # Optimization purposes, can be omitted

lib.h has to be linked as well

    b. Python

Python is executable using either python command in command line
or by changing the first line of all scripts to #!/path_to_python
and providing execution privileges for the script (chmod +x in UNIX).

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----##-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

4. Usage 

    a. Adjusting means and standard deviations of currents for new datasets

adjustTable.py

Usage: 
adjustTable.py size defaultTable.csv currents.fasta outputTable.csv

    size                - int (expected values: 3, 4, 5; should work with other positive values as well)
    defaultTable.csv    - file name (must mach the size value)
    currents.fasta      - file name
    outputTable.csv     - file name (will be created or overwritten)

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

    b. Viterbi Algorithm

Usage:
viterbi(.py) currents.fasta table.csv output.fasta size stay_prob skip_prob
    
    currents.fasta      - file name
    table.csv           - file name (must match the size value)
    output.fasta        - file name (wil be created or overwritten)
    size                - int (expected values: 3, 4, 5; should work with other positive values as well)
    stay_prob           - float <0 - 1>
    skip_prob           - float <0 - 1>

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

    c. Forward-Backward Algorithm

Usage:
fwd_bkw(.py) currents.fasta table.csv output.fasta size stay_prob skip_prob smoothing consolidate

    currents.fasta      - file name
    table.csv           - file name (must match the size value)
    output.fasta        - file name (will be created or overwritten)
    size                - int (expected values: 3, 4, 5; should work with other positive values as well)
    stay_prob           - float <0 - 1>
    skip_prob           - float <0 - 1>
    smoothing           - float <0 - 1> 
    consolidate         - y/n

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

    d. Alignment

Usage:
align(.py) reference.fasta predictions.fasta method
    
    reference.fasta     - file name
    predictions.fasta   - file name
    method              - local, global, edit

#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

    e. Check Accuracy of Alignments

Usage:
checkAcc.py predictions.fasta results.txt

    predictions.fasta   - file name (output of base-calling)
    results.txt         - file name (will be created or appended into)    
    
#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#-----#

    f. Generate Sequences with Currents (Events)

Usage:
generateGrouped.py minLen maxLen count size stay_prob skip_prob table.csv out

    minLen              - int
    maxLen              - int (>= minLen)
    count               - int
    size                - int (expected values: 3, 4, 5; should work with other positive values)
    stay_prob           - float <0 - 1>
    skip_prob           - float <0 - 1>
    table.csv           - file name (must match the size value)
    out                 - string (prefix to the generated outBases.fasta and outCurrents.fasta files)
